package libcfg_test

import (
	"testing"

	"github.com/gogriddy/libcfg"
)

type simple struct {
	Var1 string `env:"VAR_1"`
	Var2 string `env:"VAR_2"`
	Opt  string `env:"OPT,optional"`
}

type missing struct {
	VarX string `env:"VAR_X"`
	VarY string `env:"VAR_Y"`
}

type nested struct {
	Var1 string `env:"VAR_1"`
	Var2 string `env:"VAR_2"`

	Sub struct {
		SubVar1 string `env:"SUB_VAR_1"`
		SubVar2 string `env:"SUB_VAR_2"`
	}
}

type hasHidden struct {
	Var1 string `env:"VAR_1"`
	Var2 string `env:"VAR_2,secret"`
	Opt  string `env:"OPT,optional"`
}

// Ensure that secret vars aren't output by String()
func TestString(t *testing.T) {
	var v hasHidden
	s, err := libcfg.ParseEnvWithString(&v, lookupFn)
	if err != nil {
		t.Fatalf("Failed to ParseEnv: %s", err)
	}

	expected := `OPT: ""
VAR_1: "VALUE_1"
VAR_2: "********"`
	if s != expected {
		t.Errorf("Expected [%s]. Got [%s]", expected, s)
	}
}

// Validate flat struct
func TestFlatStruct(t *testing.T) {
	var v simple

	err := libcfg.ParseEnv(&v, lookupFn)
	if err != nil {
		t.Fatalf("Failed to ParseEnv: %s", err)
	}

	if v.Var1 != "VALUE_1" {
		t.Errorf("Expected Var1=VALUE_1 but got [%s]", v.Var1)
	}
	if v.Var2 != "VALUE_2" {
		t.Errorf("Expected Var2=VALUE_2 but got [%s]", v.Var1)
	}
}

// Validate nested struct
func TestNestedStruct(t *testing.T) {
	var v nested

	err := libcfg.ParseEnv(&v, lookupFn)
	if err != nil {
		t.Fatalf("Failed to ParseEnv: %s", err)
	}

	if v.Var1 != "VALUE_1" {
		t.Errorf("Expected Var1=VALUE_1 but got [%s]", v.Var1)
	}
	if v.Var2 != "VALUE_2" {
		t.Errorf("Expected Var2=VALUE_2 but got [%s]", v.Var1)
	}
	if v.Sub.SubVar1 != "SUB_VALUE_1" {
		t.Errorf("Expected Sub.SubVar1=SUB_VALUE_1 but got [%s]", v.Sub.SubVar1)
	}
	if v.Sub.SubVar2 != "SUB_VALUE_2" {
		t.Errorf("Expected Sub.SubVar2=SUB_VALUE_2 but got [%s]", v.Sub.SubVar2)
	}
}

// Validate bad nested struct
func TestBadNestedStruct(t *testing.T) {
	var v struct {
		Var1 string `env:"VAR_1"`
		Var2 string `env:"VAR_2"`

		Sub struct {
			SubBad int `env:"SUB_VAR_1"`
		}
	}

	err := libcfg.ParseEnv(&v, lookupFn)
	if errInfo, ok := err.(*libcfg.Error); !(ok && errInfo.Code() == libcfg.ErrTypeNotSupported) {
		t.Error("Expected ErrTypeNotSupported but got", err)
	}
}

// Validate env not found
func TestNotFound(t *testing.T) {
	var v missing

	err := libcfg.ParseEnv(&v, lookupFn)
	if err == nil {
		t.Fatal("Expected error but got nil")
	}
	if errInfo, ok := err.(*libcfg.Error); !(ok && errInfo.Code() == libcfg.ErrEnvNotSet) {
		t.Error("Expected ErrEnvNotSet but got", err)
	}
}

// Validate err handling for non-pointer-to-struct
func TestNotPointerToStruct1(t *testing.T) {
	v := new(int)

	err := libcfg.ParseEnv(v, lookupFn)
	if errInfo, ok := err.(*libcfg.Error); !(ok && errInfo.Code() == libcfg.ErrNeedPointerToStruct) {
		t.Error("Expected ErrNeedPointerToStruct but got", err)
	}
}

// Validate err handling for non-pointer-to-struct
func TestNotPointerToStruct2(t *testing.T) {
	var v *int

	err := libcfg.ParseEnv(v, lookupFn)
	if err != nil {
		t.Error("Expected nil but got", err)
	}
}

// Validate err handling for non-pointer-to-struct
func TestNotPointerToStruct3(t *testing.T) {
	var v int

	err := libcfg.ParseEnv(v, lookupFn)
	if errInfo, ok := err.(*libcfg.Error); !(ok && errInfo.Code() == libcfg.ErrNeedPointerToStruct) {
		t.Error("Expected ErrEnvNotSet but got", err)
	}
}

// Validate that our Errors struct implements Error()
func TestImplementsError(t *testing.T) {
	err := libcfg.NewErrNeedPointerToStruct("string")
	_ = err.Error()
}

// lookupFn provides a generic mirror of os.LookupEnv for use with testing
func lookupFn(key string) (string, bool) {
	switch key {
	case "Var1":
		return "Value1", true
	case "Var2":
		return "Value2", true

	case "VAR_1":
		return "VALUE_1", true
	case "VAR_2":
		return "VALUE_2", true
	case "SUB_VAR_1":
		return "SUB_VALUE_1", true
	case "SUB_VAR_2":
		return "SUB_VALUE_2", true
	default:
		return "", false
	}
}
