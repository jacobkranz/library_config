# libcfg
a simple library for loading config

Tag Format
----------
* env:"VAR_NAME"
* optional: declares the variable optional
* secret: obfuscates the value in the string output

Quickstart
----------

```
package main

import (
	"fmt"
	"os"

	"github.com/gogriddy/libcfg"
)

// Declare the config using the env tag to specify environment variables for each var
type config struct {
	SomeVar string `env:"VAR"`
}

func main() {
	var cfg config

	// ParseEnv return an error if one or more environment variables aren't found
	err := libcfg.ParseEnv(&cfg, os.LookupEnv)
	if err != nil {
		panic(err)
	}

	fmt.Println(cfg.SomeVar)
}
```
