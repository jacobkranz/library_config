// Package libcfg handles parsing of environment variables
package libcfg

import (
	"fmt"
	"reflect"
	"sort"
	"strings"
)

// LookupFn is used by ParseEnv to perform the lookup of an environment variable
// Note that its signature mirrors os.LookupEnv
type LookupFn func(key string) (string, bool)

// tagData encapsulates the information from a tag
type tagData struct {
	Key      string // The config / env key specified
	Optional bool   // Whether the key is optional
	Secret   bool   // Whether the key's value is secret and should not be output
}

// ParseEnv does the same as ParseEnvWithString but
func ParseEnv(s interface{}, fn LookupFn) error {
	_, err := ParseEnvWithString(s, fn)
	return err
}

// ParseEnvWithString uses the provided LookupFn to populate the given struct
func ParseEnvWithString(s interface{}, fn LookupFn) (string, error) {
	configMap := make(map[string]string)
	configString := []string{}

	// Ensure that we have pointer-to-struct
	t := reflect.TypeOf(s)
	if t.Kind() != reflect.Ptr {
		return "", NewErrNeedPointerToStruct(t.String())
	}
	if reflect.ValueOf(s).IsNil() {
		return "", nil
	}

	v := reflect.ValueOf(s).Elem()
	t = v.Type()
	if t.Kind() != reflect.Struct {
		return "", NewErrNeedPointerToStruct(t.String())
	}

	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		fv := v.Field(i)

		// If the field is a struct, recurse with a pointer to it
		if f.Type.Kind() == reflect.Struct {
			var tmpConfigString string
			var err error
			if tmpConfigString, err = ParseEnvWithString(fv.Addr().Interface(), fn); err != nil {
				return "", err
			}

			// Copy from tmpConfigMap to our local configMap
			configString = append(configString, tmpConfigString)

			continue
		} else if f.Type.Kind() != reflect.String {
			return "", NewErrTypeNotSupported(f.Type.String())
		}

		// Get the env name from the tag or use the field name
		tag := parseTag(f.Tag.Get("env"))

		// Get the env value
		value, found := fn(tag.Key)
		if !found && !tag.Optional {
			return "", NewErrEnvNotSet(tag.Key)
		}

		// Set the field value
		v.Field(i).SetString(value)

		if tag.Secret {
			configMap[tag.Key] = "********"
		} else {
			configMap[tag.Key] = value
		}

	}

	// Add the map to the strings
	for k, v := range configMap {
		configString = append(configString, fmt.Sprintf("%s: \"%s\"", k, v))
	}
	// Sort the strings
	sort.Strings(configString)

	return strings.Join(configString, "\n"), nil
}

// parseTag returns the tagData
func parseTag(tag string) tagData {
	result := tagData{}

	// Split the tag to parse the config key and any attributes
	split := strings.Split(tag, ",")
	if len(split) > 0 {
		result.Key = split[0]
	}

	// Parse attributes
	for i, v := range split {
		if i == 0 {
			result.Key = v
			continue
		}

		switch v {
		case "optional":
			result.Optional = true

		case "secret":
			result.Secret = true
		}
	}

	return result
}
