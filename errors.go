package libcfg

import "fmt"

const (
	// ErrNeedPointerToStruct indicates that an invalid data type was provided to ParseEnv
	ErrNeedPointerToStruct = iota

	// ErrEnvNotSet indicates that a required environment variable is not set
	ErrEnvNotSet

	// ErrTypeNotSupported indicates that the "env" tag is not on the struct field
	ErrTypeNotSupported
)

// Error wraps an error with details specific to this library
type Error struct {
	message string
	code    int
}

// Error returns a string version of the error message
func (e *Error) Error() string {
	return e.message
}

// Code returns the error code
func (e *Error) Code() int {
	return e.code
}

// NewErrNeedPointerToStruct creates an error with code ErrNeedPointerToStruct
func NewErrNeedPointerToStruct(kind string) error {
	return &Error{
		message: fmt.Sprintf("interface must be pointer-to-struct but have \"%s\"", kind),
		code:    ErrNeedPointerToStruct,
	}
}

// NewErrEnvNotSet creates an error with code ErrEnvNotSet
func NewErrEnvNotSet(key string) error {
	return &Error{
		message: fmt.Sprintf("expected environment variable not found: \"%s\"", key),
		code:    ErrEnvNotSet,
	}
}

// NewErrTypeNotSupported creates an error with code ErrTypeNotSupported
func NewErrTypeNotSupported(t string) error {
	return &Error{
		message: fmt.Sprintf("unsupported type: \"%s\"", t),
		code:    ErrTypeNotSupported,
	}
}
